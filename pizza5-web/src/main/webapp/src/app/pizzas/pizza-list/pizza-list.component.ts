
import {Component, OnInit} from "@angular/core";
import {Pizza} from "../shared/pizza.model";
import {PizzaService} from "../shared/pizza.service";
import {Router} from "@angular/router";


@Component({
  moduleId: module.id,
  selector: 'pizza-list',
  templateUrl: './pizza-list.component.html',
  styleUrls: ['./pizza-list.component.css'],
})

export class PizzaListComponent implements OnInit {
  pizzas: Pizza[];

  ngOnInit(): void {
    this.getPizzas();
  }

  constructor(private pizzaService: PizzaService,
              private router: Router) {
  }

  getPizzas() {
    this.pizzaService.getPizzas()
      .subscribe(
        pizzas => this.pizzas = pizzas,
      );
  }
}
