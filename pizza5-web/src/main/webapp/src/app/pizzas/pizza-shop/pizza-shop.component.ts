
import {Component, OnInit} from "@angular/core";
import {PizzaService} from "../shared/pizza.service";

@Component({
  moduleId: module.id,
  selector: 'pizza-shop',
  templateUrl: './pizza-shop.component.html',
  styleUrls: ['./pizza-shop.component.css'],
})

export class PizzaShopComponent implements OnInit {
  private errorOccurred: boolean;
  private saveDone: boolean;
  private addPressed: boolean;

  constructor(private pizzaService: PizzaService) {}

  ngOnInit(): void {
    this.addPressed = false;
    this.errorOccurred = false;
    this.saveDone = false;
  }

  add() : void {
    this.addPressed = true;
  }

  savePizza(name, description, price) : void {
    this.errorOccurred = false;
    console.log(name, description, price);
    this.pizzaService.savePizza(name, description, price)
      .subscribe(response => {
        if (Object.keys(response).length === 0 && response.constructor === Object) {
          this.errorOccurred = true;
        }
        this.saveDone = true;
        this.addPressed = false;
      });
  }
}
