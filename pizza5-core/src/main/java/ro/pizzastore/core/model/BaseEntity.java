package ro.pizzastore.core.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created on 14.06.2017.
 */
@Setter
@Getter
@ToString
@MappedSuperclass
public class BaseEntity<ID extends Serializable> implements Serializable {

    @Id
    @TableGenerator(name="TABLE_GENERATOR", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "TABLE_GENERATOR")
    @Column(unique = true, nullable = false)
    private ID id;

}
