package ro.pizzastore.web.converter;

import ro.pizzastore.core.model.BaseEntity;
import ro.pizzastore.web.dto.BaseDto;

/**
 * Created on 14.06.2017.
 */
public interface Converter<Model extends BaseEntity<Long>, Dto extends BaseDto> {
    Dto convertModelToDto(Model model);
    Model convertDtoToModel(Dto dto);
}
