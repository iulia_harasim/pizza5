import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {PizzaListComponent} from "./pizzas/pizza-list/pizza-list.component";
import {PizzaShopComponent} from "./pizzas/pizza-shop/pizza-shop.component";


const routes: Routes = [
  {path: 'pizzas', component: PizzaListComponent},
  {path: 'pizzashop5', component: PizzaShopComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
