import {Injectable} from "@angular/core";
import {Headers, Http, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Pizza} from "./pizza.model";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class PizzaService {
  private serverURL = 'http://localhost:8080/api/';
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) {

  }

  private extractData(res: Response) {
    let body = res.json();
    return body.pizzas || {};
  }

  private extractPizzaData(res: Response) {
    let body = res.json();
    return body.pizza || {};
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  getPizzas() : Observable<Pizza[]> {
    let url = this.serverURL + 'pizzas';
    return this.http.get(url)
      .map(this.extractData)
      .catch(this.handleError);
  }

  savePizza(name: string, description: string, price: number): Observable<Pizza> {
    let pizza = {name, description, price};
    return this.http.post(this.serverURL + '/savePizza', JSON.stringify({"pizza": pizza}), {headers: this.headers})
      .map(this.extractPizzaData)
      .catch(this.handleError);
  }
}
