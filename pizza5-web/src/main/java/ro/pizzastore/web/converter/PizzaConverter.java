package ro.pizzastore.web.converter;

import org.springframework.stereotype.Component;
import ro.pizzastore.core.model.Pizza;
import ro.pizzastore.web.dto.PizzaDto;

/**
 * Created on 14.06.2017.
 */
@Component
public class PizzaConverter extends BaseConverter<Pizza, PizzaDto> {

    @Override
    public PizzaDto convertModelToDto(Pizza pizza) {
        return PizzaDto.builder()
                .name(pizza.getName())
                .description(pizza.getDescription())
                .price(pizza.getPrice())
                .build();
    }

    @Override
    public Pizza convertDtoToModel(PizzaDto dto) {
        return Pizza.builder()
                .name(dto.getName())
                .description(dto.getDescription())
                .price(dto.getPrice())
                .build();
    }
}
