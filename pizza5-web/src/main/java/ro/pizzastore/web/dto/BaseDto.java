package ro.pizzastore.web.dto;

import lombok.*;

import java.io.Serializable;

/**
 * Created on 14.06.2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class BaseDto implements Serializable {
    private Long id;
}
