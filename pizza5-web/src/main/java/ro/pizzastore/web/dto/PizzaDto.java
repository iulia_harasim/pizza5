package ro.pizzastore.web.dto;

import lombok.*;

/**
 * Created on 14.06.2017.
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
@Builder
public class PizzaDto extends BaseDto {
    private String name;
    private String description;
    private float price;
}
