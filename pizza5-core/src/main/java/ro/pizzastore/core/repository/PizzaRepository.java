package ro.pizzastore.core.repository;

import org.springframework.data.jpa.repository.Query;
import ro.pizzastore.core.model.Pizza;

/**
 * Created on 14.06.2017.
 */

public interface PizzaRepository extends BaseRepository<Pizza, Long> {
    @Query("SELECT count(name) FROM Pizza WHERE name = ?1")
    Integer findByName(String name);
}
