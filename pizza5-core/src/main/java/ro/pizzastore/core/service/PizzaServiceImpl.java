package ro.pizzastore.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.pizzastore.core.model.Pizza;
import ro.pizzastore.core.repository.PizzaRepository;

import java.util.List;

/**
 * Created on 14.06.2017.
 */
@Service
public class PizzaServiceImpl implements PizzaService {

    private static final Logger log = LoggerFactory.getLogger(PizzaServiceImpl.class);

    private final PizzaRepository pizzaRepository;

    @Autowired
    public PizzaServiceImpl(PizzaRepository pizzaRepository) {
        this.pizzaRepository = pizzaRepository;
    }

    @Override
    public Pizza save(Pizza pizza) {
        log.trace("entered method");
        log.trace("saving {}", pizza);

        if (pizzaRepository.findByName(pizza.getName()) != 0) {
            log.trace("a pizza with the name {} already exists", pizza.getName());
            return null;
        }

        pizza = pizzaRepository.save(pizza);
        log.trace("pizza saved successfully");

        return pizza;
    }

    @Override
    public void delete(Long id) {
        log.trace("entered method");
        log.trace("deleting pizza with id {}", id);

        pizzaRepository.delete(id);

        log.trace("exiting method");
    }

    @Override
    @Transactional
    public Pizza update(Pizza pizza) {
        log.trace("method entered");
        log.trace("updating pizza with ID={} with {}", pizza.getId(), pizza);

        Pizza oldPizza = pizzaRepository.findOne(pizza.getId());
        oldPizza.setName(pizza.getName());
        oldPizza.setDescription(pizza.getDescription());
        oldPizza.setPrice(pizza.getPrice());

        log.trace("exiting method");
        return oldPizza;
    }

    @Override
    public List<Pizza> findAll() {
        log.trace("method entered");
        List<Pizza> pizzas = pizzaRepository.findAll();
        log.trace("pizzas={}", pizzas);
        return pizzas;
    }

    @Override
    public Pizza findOne(Long id) {
        log.trace("method entered");
        Pizza pizza = pizzaRepository.findOne(id);
        log.trace("pizza={}", pizza);
        return pizza;
    }
}
