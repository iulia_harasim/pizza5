package ro.pizzastore.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ro.pizzastore.core.model.Pizza;
import ro.pizzastore.core.service.PizzaService;
import ro.pizzastore.web.converter.PizzaConverter;
import ro.pizzastore.web.dto.PizzaDto;
import ro.pizzastore.web.dto.PizzasDto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created on 14.06.2017.
 */
@RestController
public class PizzaStoreController {
    private static final Logger log = LoggerFactory.getLogger(PizzaStoreController.class);

    private final PizzaService pizzaService;
    private final PizzaConverter pizzaConverter;

    @Autowired
    public PizzaStoreController(PizzaService pizzaService, @Qualifier("pizzaConverter") PizzaConverter pizzaConverter) {
        this.pizzaService = pizzaService;
        this.pizzaConverter = pizzaConverter;
    }

    @RequestMapping(value="/pizzas", method = RequestMethod.GET)
    public PizzasDto getAll() {
        log.trace("method entered");
        List<Pizza> pizzas = pizzaService.findAll();
        log.trace("pizzas={}", pizzaConverter.convertModelsToDtos(pizzas));
        return new PizzasDto(pizzaConverter.convertModelsToDtos(pizzas));
    }

    @RequestMapping(value="/savePizza", method = RequestMethod.POST)
    public Map<String, PizzaDto> addPizza(@RequestBody final Map<String, PizzaDto> pizzaDtoMap) {
        log.trace("method entered");
        PizzaDto dto = pizzaDtoMap.get("pizza");

        log.trace("pizzaDto to be added = {}", dto);
        Pizza pizza  = pizzaConverter.convertDtoToModel(dto);

        log.trace("pizza to be added = {}", pizza);
        pizza = pizzaService.save(pizza);

        Map<String, PizzaDto> response = new HashMap<>();
        if (pizza != null) {
            response.put("pizza", pizzaConverter.convertModelToDto(pizza));
        }

        return response;
    }
}
