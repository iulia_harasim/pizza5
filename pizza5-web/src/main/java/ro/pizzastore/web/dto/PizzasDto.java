package ro.pizzastore.web.dto;

import lombok.*;

import java.util.Set;

/**
 * Created on 14.06.2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class PizzasDto {
    private Set<PizzaDto> pizzas;

}