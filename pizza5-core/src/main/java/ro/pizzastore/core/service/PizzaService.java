package ro.pizzastore.core.service;

import ro.pizzastore.core.model.Pizza;

import java.util.List;

/**
 * Created on 14.06.2017.
 */
public interface PizzaService {
    Pizza save(Pizza pizza);
    void delete(Long id);
    Pizza update(Pizza pizza);
    List<Pizza> findAll();
    Pizza findOne(Long id);
}
