import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";

import {AppComponent} from "./app.component";
import {AppRoutingModule} from "./app-routing.module";
import {PizzaService} from "./pizzas/shared/pizza.service";
import {PizzaListComponent} from "./pizzas/pizza-list/pizza-list.component";
import {PizzaShopComponent} from "./pizzas/pizza-shop/pizza-shop.component";

@NgModule({
  declarations: [
    AppComponent,
    PizzaListComponent,
    PizzaShopComponent
  ],

  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
  ],
  providers: [PizzaService],
  bootstrap: [AppComponent]
})
export class AppModule {
}


